<!DOCTYPE html>
<html lang="de">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>@yield('pageTitle',config('app.name'))</title>
    <link rel="stylesheet" href="/css/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/js/toastr-master/build/toastr.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>

<body>
    <div id="wrapper" class="container-fluid">
        @yield('content')
    </div>

    {{-- Bootstrap- Modal --}}
    </div>
    <div class="modal fade" id="deleteModal" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteModalLabel">Name</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="deleteModalBody">
                    {{-- Wirklich löschen? --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>
                    <button type="button" class="btn btn-danger">Löschen</button>
                </div>
            </div>
        </div>
    </div>

    <script src="/js/jquery-3.5.1.min.js"></script>
    <script src="/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/js/toastr-master/build/toastr.min.js"></script>
    <script src="/js/script.js"></script>

    @if (View::hasSection('jsScript'))
        <script>
            "use strict";
            (function($) {
                $(document).ready(function() {
                    @yield('jsScript')
                })
            })(jQuery)

        </script>
    @endif
</body>

</html>

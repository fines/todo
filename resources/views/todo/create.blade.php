@extends('layouts.main')
@section('pageTitle')

@section('content')

    <h1>Neue Todo Liste</h1>

    <div class="button"><a href="{{ route('todo.index') }}" class="btn btn-outline-secondary">Alle Todo Listen</a></div>

    <div id="form" class="form">
        <form action="{{ route('todo.store') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                    value="{{ old('name') }}">
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-dark">speichern</button>
        </form>
    </div>

@endsection

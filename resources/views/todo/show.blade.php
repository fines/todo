@extends('layouts.main')
@section('pageTitle')

@section('content')

    <div class="button"><a href="{{ route('todo.index') }}" class="btn btn-outline-secondary">Alle Todo-Listen</a></div>

    <h1>{{ $todo->name }}</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>

            @foreach ($todoitems as $item)
                <tr>
                    <th scope="row">{{ $item->id }}</th>
                    <td>{{ $item->name }}</td>
                    <td><a href="{{ route('todoitem.edit', $item->id) }}" class="btn btn-outline-dark fa fa-edit"></a>
                    </td>
                    <td>
                        <form action="{{ route('todoitem.destroy', $item->id) }}" method="post" class="delete"
                            data-title="{{ $item->name }}"
                            data-body=" Soll das Todo- Item <strong>{{ $item->name }}</strong> gelöscht werden?">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-outline-danger fa fa-check-circle"></button>
                        </form>
                    </td>
                </tr>
            @endforeach

            {{-- <tr>
                <th scope="row">1</th>
                <td>HTML/CSS</td>
                <td><a href="../todoitem/edit.html" class="btn btn-outline-dark fa fa-edit"></a></td>
                <td>
                    <button type="submit" class="btn btn-outline-danger fa fa-trash"></button>
                </td>
            </tr>
            <tr>
                <th scope="row">1</th>
                <td>Backend</td>
                <td><a href="../todoitem/edit.html" class="btn btn-outline-dark fa fa-edit"></a></td>
                <td>
                    <button type="submit" class="btn btn-outline-danger fa fa-trash"></button>
                </td>
            </tr> --}}
        </tbody>
    </table>
    <br>
    <br>
    <h2>Neues Todo Item</h2>

    <div id="form" class="form">
        <form action="{{ route('todoitem.store') }}" method="POST">
            @csrf
            <div class="form-group">
                <label for="name">Todo-Item Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                    value="{{ old('name') }}">
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
                {{-- Übermitteln der id des akt. zugehörigen Todos: --}}
                <input type="text" hidden class="form-control" name="todo_id" id="todo_id" value="{{ $todo->id }}">

            </div>
            <button type="submit" class="btn btn-dark">speichern</button>
        </form>
    </div>

@endsection

@extends('layouts.main')
@section('pageTitle')

@section('content')

    <div class="button"><a href="{{ route('todo.create') }}" class="btn btn-outline-secondary">Neue ToDo Liste</a></div>
    <h1>Todo-Listen</h1>
    <table class="table table-striped">
        <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Name</th>
                <th scope="col">Erstellungsdatum</th>
                <th scope="col">Todo Items</th>
                <th scope="col"></th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
        </thead>
        <tbody>

            {{-- foreach-> DB --}}
            @foreach ($todos as $todo)
                <tr>
                    <th scope="row">{{ $todo->id }}</th>
                    <td>{{ $todo->name }}</td>
                    {{-- Datum formatieren: --}}
                    {{-- AppServiceProvider.php ->  setLocale f Carbon --}}
                    <td>{{ $todo->created_at->formatLocalized('%a, %d.%m.%Y') }}</td>
                    <td>
                        <ul>
                            @foreach ($todo->todoitems as $item)
                                <li>{{ $item->name }}</li>
                            @endforeach
                        </ul>
                    </td>
                    <td><a href="{{ route('todo.show', $todo->id) }}" class="btn btn-outline-dark fa fa-eye"></a></td>
                    <td><a href="{{ route('todo.edit', $todo->id) }}" class="btn btn-outline-dark fa fa-edit"></a></td>
                    <td>
                        @if ($todo->todoitems_count < 1)
                            <form action="{{ route('todo.destroy', $todo->id) }}" method="POST" class="delete"
                                data-title="{{ $todo->name }}"
                                data-body="Soll die Todo- Liste <strong>{{ $todo->name }}</strong> wirklich gelöscht werden?">
                                @method('delete')
                                @csrf
                                {{-- font awesome icon: fa-trash --}}
                                <button type="submit" class="btn btn-outline-danger fa fa-check-circle"></button>
                            </form>
                        @endif
                    </td>
                </tr>
            @endforeach

        </tbody>
    </table>
@endsection

@if (session('success'))
    @section('jsScript')
        myToastr('success','{{ session('success') }}');
    @endsection
@endif

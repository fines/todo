@extends('layouts.main')
@section('pageTitle', 'edit ToDo')

@section('content')
    <h1>Todo Liste ändern</h1>
    <div class="button"><a href="{{ route('todo.index') }}" class="btn btn-outline-secondary">Alle Todo-Listen</a></div>

    <div id="form" class="form">

        <form action="{{ route('todo.update', $todo->id) }}" method="POST">
            @method('put')
            @csrf

            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                    value="{{ old('name', $todo->name) }}">
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror
            </div>
            <button type="submit" class="btn btn-dark">ändern</button>
        </form>
    </div>
@endsection

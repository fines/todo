@extends('layouts.main')
@section('pageTitle')

@section('content')

    <h1>Todo Item ändern</h1>
    <div class="button"><a href="{{ route('todo.index') }}" class="btn btn-outline-secondary">Alle DoTo's</a></div>

    <div id="form" class="form">
        <form action="{{ route('todoitem.update', $todoitem->id) }}" method="post">
            @method('put')
            @csrf
            <div class="form-group">
                <label for="title">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="name"
                    value="{{ old('name', $todoitem->name) }}">
                @error('name')
                    <div class="invalid-feedback">{{ $message }}</div>
                @enderror

                <input type="text" hidden class="form-control" name="id" id="id" value="{{ $todoitem->id }}">
                <input type="text" hidden class="form-control" name="todo_id" id="todo_id"
                    value="{{ $todoitem->todo_id }}">
            </div>
            <button type="submit" class="btn btn-dark">speichern</button>
        </form>
    </div>
@endsection

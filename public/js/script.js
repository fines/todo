"use strict";

(function($){
    $(document).ready(function(){

        //TOASTR
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": false,
            "progressBar": false,
            "positionClass": "toast-top-full-width",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        }

        window.myToastr = function(typ,msg,title=undefined){
            switch(typ){
                case 'success' : toastr.success(msg, title || 'Erfolg'); break;
                case 'info' : toastr.info(msg, title || 'Erfolg'); break;
                case 'warning' : toastr.warning(msg, title || 'Erfolg'); break;
                case 'error' : toastr.error(msg, title || 'Erfolg'); break;
            }
        }

        $('.delete').on('submit',function(e){
            e.preventDefault();
            const form = $(this),
                    deleteModal = $('#deleteModal');

            deleteModal.modal("show");
    
            $('#deleteModalLabel').html(form.data('title'));
            $('#deleteModalBody').html(form.data('body'));

            $('#deleteModal .btn.btn-danger').off().on('click',function(){
                deleteModal.modal("hide");
                //console.log(form.attr('action'));
                console.log(form.serialize());
                $.ajax({
                    url : form.attr('action'),
                    method : 'DELETE',
                    data : form.serialize(),
                    success : function(response){
                        if( response.status === 200 ){
                            form.closest('tr').remove();
                            myToastr('success',response.msg);
                        }
                        else{
                            myToastr('error',response.msg,response.status);
                        }
                    },
                    error : function(xhr){
                        console.log(xhr.status, xhr.statusText );
                    }

                });
            })
        });

    });
})(jQuery);
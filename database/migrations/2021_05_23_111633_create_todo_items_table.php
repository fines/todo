<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTodoItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('todo_items', function (Blueprint $table) {
            $table->id();
            //$table->unsignedBigInteger('todo_id')->nullable();
            // //todos können nur gelöscht werden wenn keine Unteraufgaben vorhanden sind.
            //$table->foreign('todo_id')->references('id')->on('todos')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('todo_id')->constrained();
            $table->string('name');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('todo_items');
    }
}
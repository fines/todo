<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TodoItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'todo_id',
        'name'
    ];
    
    // Def. Tabellenverhältnisse TodoItem : Todo == n : 1
    public function todo(){
        return $this->belongsTo(Todo::class,'id','todo_id');
    }
    
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;

    protected $fillable= [
        'name'
    ];
    
    // Todo:Todoitems == 1:n
    public function todoitems(){
        return $this->hasMany(TodoItem::class,'todo_id','id');
    }
}
<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use App\Models\TodoItem;
use Illuminate\Http\Request;

class TodoItemController extends Controller
{
    public $rules=[
        'name'=> 'required|min:3|unique:todo_items,name',
        //'todo_id' => 'exists:todos,id'
    ];
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Todo $todo)
    {
        //dd($request->all());
        $request->validate($this->rules);
        //TodoItem::create(['name'=>$request->name, 'todo_id'=>$request? ])
        //$new=
        TodoItem::create($request->all());
        //dd($new);
        return back()->with('success','Neue Unteraufgabe '.$request->name.' erstellt');
    }
/**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Todoitem  $todoitem
     * @return \Illuminate\Http\Response
     */
    public function edit(TodoItem $todoitem)
    {
        return view('todoitem.edit',compact('todoitem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\TodoItem  $todoitem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, TodoItem $todoitem)
    {
        //Validierung
        $request->validate($this->rules);
        //Updaten
        $todoitem->update(['name' => $request->name]);
        //dd('update item');
        return redirect()->route('todo.show',$request->todo_id)->with('success','Unteraufgabe '.$request->name.' wurde geändert.');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $todoitem=TodoItem::find($id);
        
        if($todoitem){
            $status=200;
            $key= 'success';
            $msg='Unteraufgabe '.$todoitem->name.' wurde erledigt!';
            TodoItem::destroy($id);
        }
        else {
            $status=404;
            $key='error';
            $msg = 'Unteraufgabe nicht gefunden!';
        }
        
        if(request()->ajax() ){
            return response()->json(compact('status','msg'));
        }
        return back()->with($key, $msg);
    }

}
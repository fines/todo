<?php

namespace App\Http\Controllers;

use App\Models\Todo;
use App\Models\TodoItem;
use Illuminate\Http\Request;

class TodoController extends Controller
{
    public $rules=[
        'name' => 'required|min:3|unique:todos,name'
    ]; // Todo- Titel
    
    /**
     * Alle Todos bzw. Projekte anzeigen.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // SQL SELECT * FROM todos ORDER BY (created_at oder name)
        //$todos= Todo::select()->orderBy('created_at','desc')->withCount('todoitems')->get(); // Todoitems in Abfrage einbinden! (PK)
        $todos= Todo::with('todoitems')->select()->orderBy('created_at','desc')->withCount('todoitems')->get();
        
        return view('todo.index',compact('todos'));
    }

    /**
     * Zeige Formular zum Erstellen neuer Todos.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('todo.create'); //oder todo.edit wenn ein Formular für beides
    }

    /**
     * Neues Todo speichern.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // 1. Validation
        $request->validate($this->rules); //rules s oben
        $todo=[ 'name' => $request->name];
        Todo::create($todo);

        return redirect()->route('todo.index')->with('success','Neues Todo- Item '.$request->name.' erstellt!');
    }

    /**
     * Einzelnes Todo anzeigen.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function show(Todo $todo)
    {
        $todoitems= TodoItem::where('todo_id',$todo->id)->get(); //Eloquent- collection
        //dd(compact('todoitems'),$todoitems, $todo);
        return view('todo.show', compact('todo','todoitems'));
    }

    /**
     * Formular zeigen zum bearbeiten eines spezifischen Todos.
     *
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function edit(Todo $todo)
    {
        return view('todo.edit',compact('todo'));
    }

    /**
     * Todo ändern
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Todo  $todo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Todo $todo)
    {
        $request->validate($this->rules);

        $todo->update([ 'name' => $request->name ]);

        return redirect()->route('todo.index')->with('success','Aufgabe '.$request->name .' geändert.');
    }

    /**
     * Spezifisches Todo löschen.
     *
     * @param  \App\Models\Todo $todo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        //$todo=Todo::select($id)->withCount('todoitems'); //liefert Eloquent Builder"
        //$todo=Todo::find($id)->withCount('todoitems'); //Builder
        $todo=Todo::withCount('todoitems')->find($id);
        //dd($todo, $todo->todoitem_count);
        
        //Löschen soll nur möglich sein wenn keine Unteraufgaben vorhanden sind
        if ($todo && $todo->todoitems_count < 1){
            Todo::destroy($id);
            $status=200;
            $key='success';
            $msg= $todo->name.' erledigt!';
        }
        else if ($todo){
            $status=470;
            $key='warning';
            $msg='Die Aufgabe darf noch nicht gelöscht werden!';
        }
        else {
            $status=480;
            $key='error';
            $msg='Aufgabe nicht gefunden';
        }
        
        if ( request()-> ajax() ){
            return response()->json(['status'=>$status,'key'=>$key ,'msg'=>$msg]);
        }
        
        return redirect()->route('todo.index')->with($key,$msg);
    }
}